 set nocompatible               " be iMproved
 filetype off                   " required!

 set rtp+=~/.vim/bundle/vundle/
 call vundle#rc()

 " let Vundle manage Vundle
 " required!
 Bundle 'gmarik/vundle'

 " My Bundles here:
 "
 " original repos on github
 "=== vim-fugitive: Gstatus, Gdiff, Glog, Gblame ===
 Bundle 'tpope/vim-fugitive'
 "=== vim-easymotion: <Leader><Leader>w, <Leader><Leader>b ===
 Bundle 'Lokaltog/vim-easymotion'
 "=== vim-scripts repos ===
 Bundle 'L9'
 Bundle 'FuzzyFinder'
 Bundle 'ctags.vim'
 "=== make sure apt-get install ctags before install taglsit.vim ===
 Bundle 'taglist.vim'
 Bundle 'The-NERD-tree'
 Bundle 'AutoComplPop'
 Bundle 'Pydiction'
 "=== html ===
 Bundle 'html-improved-indentation'
 " non github repos
 " Bundle 'git://git.wincent.com/command-t.git'
 " ...

 filetype plugin indent on     " required!
 "
 " Brief help
 " :BundleList          - list configured bundles
 " :BundleInstall(!)    - install(update) bundles
 " :BundleSearch(!) foo - search(or refresh cache first) for foo
 " :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
 "
 " see :h vundle for more details or wiki for FAQ
 " NOTE: comments after Bundle command are not allowed..
 let g:pydiction_location = '~/.vim/bundle/Pydiction/complete-dict'


set nu
syntax on
set showmode
set hlsearch
"=== no c indent for python ===
set nocindent

"=== for Tlist auto-close when only Tlist ===
let Tlist_Show_One_File=1 
let Tlist_Exit_OnlyWindow=1 

noremap <F1> gT<CR>
noremap <F2> gt<CR>
noremap <F3> :TlistToggle <CR> 
noremap <F4> :NERDTreeToggle<CR> 

"noremap <C-c> "ay
"noremap <C-v> "ap

se cursorline
hi cursorLine term=none cterm=none ctermbg=4
autocmd InsertLeave * hi cursorLine term=none cterm=none ctermbg=4
autocmd InsertEnter * hi cursorLine term=none cterm=none ctermbg=0
hi comment term=none cterm=none ctermbg=none ctermfg=darkgray

set laststatus=2
set statusline=%4*%=\ %1*[%F]
set statusline+=%4*\ %5*[%{&encoding}, " encoding
set statusline+=%{&fileformat}%{\"\".((exists(\"+bomb\")\ &&\ &bomb)?\",BOM\":\"\").\"\"}]%m
set statusline+=%4*%=\ %6*%y%4*\ %3*%l%4*,\ %3*%c%4*\ \<\ %2*%P%4*\ \>
highlight User1 cterm=bold ctermfg=white ctermbg=blue
highlight User2 term=underline cterm=underline ctermfg=green
highlight User3 term=underline cterm=underline ctermfg=yellow
highlight User4 term=underline cterm=underline ctermfg=white
highlight User5 ctermfg=cyan
highlight User6 ctermfg=white
"=== set tab key = 4 space key ===
set ts=4
set expandtab
